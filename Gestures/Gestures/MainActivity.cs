﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RotationGesture;

namespace Gestures
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : Activity, GestureDetector.IOnGestureListener, ScaleGestureDetector.IOnScaleGestureListener, IOnRotationGestureListener
    {
        private ScaleGestureDetector scaleGestureDetector;
        private float scale = 1.0f;

        private GestureDetector gestureDetector;
        private ImageView xamLogo;
        private float deltaX, deltaY;

        private RotationGestureDetector rotationGestureDetector;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            xamLogo = FindViewById<ImageView>(Resource.Id.xamLogo);

            gestureDetector = new GestureDetector(this, this);
            scaleGestureDetector = new ScaleGestureDetector(this, this);
            rotationGestureDetector = new RotationGestureDetector(this);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public bool OnDown(MotionEvent e)
        {
            return false;
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            return false;
        }

        public void OnLongPress(MotionEvent e)
        {
            
        }

        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            deltaX -= distanceX;
            deltaY -= distanceY;

            xamLogo.TranslationX = deltaX;
            xamLogo.TranslationY = deltaY;

            return true;
        }

        public void OnShowPress(MotionEvent e)
        {
        }

        public bool OnSingleTapUp(MotionEvent e)
        {
            return false;
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            gestureDetector.OnTouchEvent(e);
            scaleGestureDetector.OnTouchEvent(e);
            rotationGestureDetector.OnTouchEvent(e);

            return true;
        }

        public bool OnScale(ScaleGestureDetector detector)
        {
            scale *= detector.ScaleFactor;

            xamLogo.ScaleX = scale;
            xamLogo.ScaleY = scale;

            return true;
        }

        public bool OnScaleBegin(ScaleGestureDetector detector)
        {
            return true;
        }

        public void OnScaleEnd(ScaleGestureDetector detector)
        {
        }

        public void OnRotate(float angle)
        {
            xamLogo.Rotation = angle;
        }
    }
}